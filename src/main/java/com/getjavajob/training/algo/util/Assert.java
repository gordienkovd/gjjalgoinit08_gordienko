package com.getjavajob.training.algo.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Assert {
    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (Objects.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, List expected, List actual) {
        if (Arrays.toString(expected.toArray()).equals(Arrays.toString(actual.toArray()))) {
            System.out.println(testName + " passed.");
        } else {
            System.out.println(testName + " failed:" + '\n' + "expected " + Arrays.toString(expected.toArray()) + '\n' + "actual   " + Arrays.toString(actual.toArray()));

        }
    }

    public static void assertEquals(String testName, Map expected, Map actual) {
        if (expected.entrySet().containsAll(actual.entrySet()) && actual.entrySet().containsAll(expected.entrySet())) {
            System.out.println(testName + " passed.");
        } else {
            System.out.println(testName + " failed:" + '\n' + "expected " + expected.toString() + '\n' + "actual   " + actual.toString());

        }
    }
}
