package com.getjavajob.training.algo.util;

import java.util.Scanner;

public class ConsoleHelper {

    public static void println(Object message) {
        System.out.println(message);
    }

    public static String readLine() {
        Scanner scanner = new Scanner(System.in, "UTF-8");
        return scanner.nextLine();
    }

    public static double readDouble() {
        Scanner scanner = new Scanner(System.in, "UTF-8");
        return scanner.nextDouble();
    }

    public static void print(Object message){
        System.out.print(message);
    }

    private static boolean isNumber(String string) {
        return string != null && string.matches("^-?\\d+$");
    }

    public static boolean isInRange(int num, int fromNumber, int toNumber) {
        return num > fromNumber & num < toNumber;
    }

    private static boolean isNumberFrom(int num, int fromNumber) {
        return num > fromNumber;
    }

    public static int getIntNumberInRange(int fromNumber, int toNumber) {
        boolean toReadAString = true;
        String resultString = "";
        while (toReadAString) {
            println("Enter a number from " + (fromNumber + 1) + " to " + (toNumber - 1) + " inclusive");
            String readTheLine = readLine();
            if (isNumber(readTheLine)) {
                int readTheNumber = Integer.parseInt(readTheLine);
                if (isInRange(readTheNumber, fromNumber, toNumber)) {
                    toReadAString = false;
                    resultString = readTheLine;
                } else {
                    println("You entered a number incorrectly, enter the number from " + (fromNumber + 1) + " to " + (toNumber - 1) + " inclusive");
                }
            } else {
                println("You entered not a number, enter a number from " + (fromNumber + 1) + " to " + (toNumber - 1) + " inclusive");
            }
        }
        return Integer.parseInt(resultString);
    }

    public static int getIntNumber() {
        boolean toReadAString = true;
        String resultString = "";
        while (toReadAString) {
            String readTheLine = readLine();
            if (isNumber(readTheLine)) {
                toReadAString = false;
                resultString = readTheLine;
            } else {
                println("You entered not a number, enter a number");
            }
        }
        return Integer.parseInt(resultString);
    }

    public static int getIntNumberFrom(int fromNumber) {
        boolean toReadAString = true;
        String resultString = "";
        while (toReadAString) {
            println("Enter a number from " + (fromNumber + 1) + " inclusive");
            String readTheLine = readLine();
            if (isNumber(readTheLine)) {
                int readTheNumber = Integer.parseInt(readTheLine);
                if (isNumberFrom(readTheNumber, fromNumber)) {
                    toReadAString = false;
                    resultString = readTheLine;
                } else {
                    println("You entered a number incorrectly, enter the number from " + (fromNumber + 1) + " inclusive");
                }
            } else {
                println("You entered not a number, enter a number from " + (fromNumber + 1) + " inclusive");
            }
        }
        return Integer.parseInt(resultString);
    }

    public static int getIntNumberInRange2(int fromNumber, int toNumber) {
        boolean toReadAString = true;
        String resultString = "";
        while (toReadAString) {
            String readTheLine = readLine();
            if (isNumber(readTheLine)) {
                int readTheNumber = Integer.parseInt(readTheLine);
                if (isInRange(readTheNumber, fromNumber, toNumber)) {
                    toReadAString = false;
                    resultString = readTheLine;
                } else {
                    println("You entered a number incorrectly, enter the number from " + (fromNumber + 1) + " to " + (toNumber - 1) + " inclusive");
                }
            } else {
                println("You entered not a number, enter a number from " + (fromNumber + 1) + " to " + (toNumber - 1) + " inclusive");
            }
        }
        return Integer.parseInt(resultString);
    }

    public static void printMatrix(int[][] matrix){
        for (int[] element : matrix) {
            for (int j = 0; j < matrix.length; j++) {
                if (j == matrix.length - 1) {
                    println(element[j]);
                } else {
                    print(element[j]);
                }
            }
        }
    }

    public static void printMatrix2(int[][] matrix, int height, int width){
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (j == width - 1) {
                    println(matrix[i][j]);
                } else {
                    print(matrix[i][j] + " ");
                }
            }
        }
    }
}
