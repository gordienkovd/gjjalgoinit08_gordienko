package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.printMatrix;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh12N023 {
    public static void main(String[] args) {
        int[][] matrix = getVariantA(new int[11][11]);
        int[][] matrix1 = getVariantB(new int[11][11]);
        int[][] matrix2 = getVariantC(new int[11][11]);
        printMatrix(matrix);
        println("");
        printMatrix(matrix1);
        println("");
        printMatrix(matrix2);
    }

    static int[][] getVariantA(int[][] numNum) {
        for (int i = 0; i < numNum.length; i++) {
            numNum[i][i] = 1;
        }
        for (int j = numNum.length - 1, i = 0; j != -1 & i < numNum.length; i++, j--) {
            numNum[i][j] = 1;
        }
        return numNum;
    }

    static int[][] getVariantB(int[][] numNum) {
        numNum = getVariantA(numNum);
        for (int j = numNum.length / 2, i = 0; i < numNum.length; i++) {
            numNum[i][j] = 1;
        }
        for (int j = 0, i = numNum.length / 2; j < numNum.length; j++) {
            numNum[i][j] = 1;
        }
        return numNum;
    }

    static int[][] getVariantC(int[][] numNum) {
        int first = -1;
        int last = numNum.length;
        for (int i = 0; i < numNum.length; i++) {
            if (i >= numNum.length / 2 + 1) {
                first--;
                last++;
            } else {
                first++;
                last--;
            }
            for (int j = 0; j < numNum.length; j++) {
                if (j >= first & j <= last) {
                    numNum[i][j] = 1;
                }
            }
        }
        return numNum;
    }
}
