package com.getjavajob.training.algo.init.gordienkov;


import static com.getjavajob.training.algo.init.gordienkov.TaskCh09N015.getChar;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N015Test {
    public static void main(String[] args) {
        testGetChar();
    }

    private static void testGetChar() {
        assertEquals("TaskCh09N015Test.getChar", 'e', getChar("test", 2));
    }
}
