package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static java.lang.Math.*;

class TaskCh06N008 {
    public static void main(String[] args) {
        println("Enter number");
        int num = getIntNumber();
        int[] numbers = new int[num];
        fillTheArray(numbers);
        println(getNumbers(numbers));
    }

    static void fillTheArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) pow(i + 1, 2);
        }
    }

    /**
     *
     * @param numbers - array of numbers of degree 2
     * @return - numbers that do not exceed the length of the numbers array
     */
    static String getNumbers(int[] numbers) {
        StringBuilder result = new StringBuilder();
        for (int element : numbers) {
            if (element <= numbers.length) {
                result.append(element).append(' ');
            }
        }
        return result.toString().trim();
    }
}
