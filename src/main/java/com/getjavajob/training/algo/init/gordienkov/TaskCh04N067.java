package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumberInRange;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh04N067 {
    public static void main(String[] args) {
        println(checkItIsWorkdayOrWeekend(getIntNumberInRange(0, 366)));
    }

    static String checkItIsWorkdayOrWeekend(int day) {
        String dayOfWeek = "Weekend";
        if (day % 7 > 0 && day % 7 < 6) {
            dayOfWeek = "Workday";
        }
        return dayOfWeek;
    }
}
