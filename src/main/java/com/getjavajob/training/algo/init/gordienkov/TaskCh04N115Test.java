package com.getjavajob.training.algo.init.gordienkov;


import static com.getjavajob.training.algo.init.gordienkov.TaskCh04N115.getAnimal;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh04N115.getColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N115Test {
    public static void main(String[] args) {
        testGetAnimalRat();
        testGetAnimalCow();
        testGetAnimalTiger();
        testGetAnimalHare();
        testGetAnimalDragon();
        testGetAnimalSnake();
        testGetAnimalHorse();
        testGetAnimalSheep();
        testGetAnimalMonkey();
        testGetAnimalCock();
        testGetAnimalDog();
        testGetAnimalPig();
        testColorGreen0();
        testColorGreen1();
        testColorRed0();
        testColorRed1();
        testColorYellow0();
        testColorYellow1();
        testColorWhite0();
        testColorWhite1();
        testColorBlack0();
        testColorBlack1();
    }

    private static void testGetAnimalRat() {
        assertEquals("TaskCh04N115Test.testGetAnimalRat", "Rat", getAnimal(1984));
    }

    private static void testGetAnimalCow() {
        assertEquals("TaskCh04N115Test.testGetAnimalCow", "Cow", getAnimal(1985));
    }

    private static void testGetAnimalTiger() {
        assertEquals("TaskCh04N115Test.testGetAnimalTiger", "Tiger", getAnimal(1986));
    }

    private static void testGetAnimalHare() {
        assertEquals("TaskCh04N115Test.testGetAnimalHare", "Hare", getAnimal(1987));
    }

    private static void testGetAnimalDragon() {
        assertEquals("TaskCh04N115Test.testGetAnimalDragon", "Dragon", getAnimal(1988));
    }

    private static void testGetAnimalSnake() {
        assertEquals("TaskCh04N115Test.testGetAnimalSnake", "Snake", getAnimal(1989));
    }

    private static void testGetAnimalHorse() {
        assertEquals("TaskCh04N115Test.testGetAnimalHorse", "Horse", getAnimal(1990));
    }

    private static void testGetAnimalSheep() {
        assertEquals("TaskCh04N115Test.testGetAnimalSheep", "Sheep", getAnimal(1991));
    }

    private static void testGetAnimalMonkey() {
        assertEquals("TaskCh04N115Test.testGetAnimalMonkey", "Monkey", getAnimal(1992));
    }

    private static void testGetAnimalCock() {
        assertEquals("TaskCh04N115Test.testGetAnimalCock", "Cock", getAnimal(1993));
    }

    private static void testGetAnimalDog() {
        assertEquals("TaskCh04N115Test.testGetAnimalDog", "Dog", getAnimal(1994));
    }

    private static void testGetAnimalPig() {
        assertEquals("TaskCh04N115Test.testGetAnimalPig", "Pig", getAnimal(1995));
    }

    private static void testColorGreen0() {
        assertEquals("TaskCh04N115Test.testColorGreen0", "Green", getColor(1984));
    }

    private static void testColorGreen1() {
        assertEquals("TaskCh04N115Test.testColorGreen1", "Green", getColor(1985));
    }

    private static void testColorRed0() {
        assertEquals("TaskCh04N115Test.testColorRed0", "Red", getColor(1986));
    }

    private static void testColorRed1() {
        assertEquals("TaskCh04N115Test.testColorRed1", "Red", getColor(1987));
    }

    private static void testColorYellow0() {
        assertEquals("TaskCh04N115Test.testColorYellow0", "Yellow", getColor(1988));
    }

    private static void testColorYellow1() {
        assertEquals("TaskCh04N115Test.testColorYellow1", "Yellow", getColor(1989));
    }

    private static void testColorWhite0() {
        assertEquals("TaskCh04N115Test.testColorWhite0", "White", getColor(1990));
    }

    private static void testColorWhite1() {
        assertEquals("TaskCh04N115Test.testColorWhite1", "White", getColor(1991));
    }

    private static void testColorBlack0() {
        assertEquals("TaskCh04N115Test.testColorBlack0", "Black", getColor(1992));
    }

    private static void testColorBlack1() {
        assertEquals("TaskCh04N115Test.testColorBlack1", "Black", getColor(1993));
    }
}
