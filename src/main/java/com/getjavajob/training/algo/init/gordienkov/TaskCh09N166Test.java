package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh09N166.replaceWords;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N166Test {
    public static void main(String[] args) {
        testReplaceWords();
    }

    private static void testReplaceWords() {
        assertEquals("TaskCh09N166Test.testReplaceWords", "task test", replaceWords("test task"));
    }
}
