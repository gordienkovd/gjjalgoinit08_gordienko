package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh05N064.getAverageDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N064Test {
    public static void main(String[] args) {
        testGetAverageDensity();
    }

    private static void testGetAverageDensity() {
        District[] actual = new District[12];
        actual[0] = new District(100, 1000);
        actual[1] = new District(200, 2000);
        actual[2] = new District(300, 3000);
        actual[3] = new District(400, 4000);
        actual[4] = new District(500, 5000);
        actual[5] = new District(600, 6000);
        actual[6] = new District(700, 7000);
        actual[7] = new District(800, 8000);
        actual[8] = new District(900, 9000);
        actual[9] = new District(1000, 10000);
        actual[10] = new District(1100, 11000);
        actual[11] = new District(1200, 12000);
        assertEquals("TaskCh05N038Test.testGetAverageDensity", 10, getAverageDensity(actual));
    }
}
