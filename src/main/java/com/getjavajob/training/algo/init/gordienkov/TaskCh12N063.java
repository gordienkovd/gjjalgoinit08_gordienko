package com.getjavajob.training.algo.init.gordienkov;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh12N063 {
    public static void main(String[] args) {
        println("Enter number of parallels");
        int countOfParallel = getIntNumber();
        println("Enter number of school classes");
        int schoolClasses = getIntNumber();
        int[][] matrix = new int[schoolClasses][countOfParallel];
        for (int i = 0, count = 1; i < schoolClasses; i++) {
            for (int j = 0; j < countOfParallel; j++) {
                matrix[i][j] = count;
                count++;
            }
        }
        Map<Integer, Integer> resultAverageNumOfStudentsOfAllParallels = getAverageNumOfStudents (matrix, countOfParallel, schoolClasses);
        for(Map.Entry<Integer,Integer> element : resultAverageNumOfStudentsOfAllParallels.entrySet()) {
            println("School class: " + element.getKey() + " , average number of students: " + element.getValue());
        }
    }

    static Map<Integer, Integer> getAverageNumOfStudents(int[][] matrix, int countOfParallel, int schoolClasses) {
        int averageNum = 0;
        Map<Integer, Integer> averageNumOfStudentsOfAllParallels = new HashMap<>(countOfParallel);
        for (int i = 0; i < schoolClasses; i++) {
            for (int j = 0; j < countOfParallel; j++) {
                averageNum += matrix[i][j];
            }
            averageNumOfStudentsOfAllParallels.put(i + 1, averageNum / countOfParallel);
            averageNum = 0;
        }
        return averageNumOfStudentsOfAllParallels;
    }
}
