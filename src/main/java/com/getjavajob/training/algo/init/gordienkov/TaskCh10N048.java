package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N048 {
    public static void main(String[] args) {
        println("Enter the size of the array");
        int lengthOfArray = getIntNumber();
        int[] numbers = new int[lengthOfArray];
        for (int i = 0; i < lengthOfArray; i++) {
            numbers[i] = i;
        }
        println(maxValueOfArr(numbers, 0, numbers[0]));
    }

    static int maxValueOfArr(int[] numbers, int count, int firstElementOfArr) {
        if (count == numbers.length) {
            return firstElementOfArr;
        } else {
            if (firstElementOfArr < numbers[count]) {
                firstElementOfArr = numbers[count];
            }
            count++;
            return maxValueOfArr(numbers, count, firstElementOfArr);
        }
    }
}
