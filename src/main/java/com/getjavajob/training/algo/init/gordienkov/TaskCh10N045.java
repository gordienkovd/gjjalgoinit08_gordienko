package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N045 {
    public static void main(String[] args) {
        println("Enter the first term of an arithmetic progression");
        int firstTerm = getIntNumber();
        println("Enter difference of an arithmetic progression");
        int difference = getIntNumber();
        println("Enter the term of an arithmetic progression");
        int term = getIntNumber();
        println(getMemberOfArithmeticProgression(firstTerm, difference, term));
        println(getSumOfMembersOfArithmeticProgression(firstTerm, difference, term));
    }

    static int getMemberOfArithmeticProgression(int firstTerm, int difference, int term) {
        if (term == 1) {
            return firstTerm;
        } else {
            return getMemberOfArithmeticProgression(firstTerm + difference, difference, term - 1);
        }
    }

    static int getSumOfMembersOfArithmeticProgression(int firstTerm, int difference, int term) {
        if (term == 1) {
            return firstTerm;
        } else {
            return getSumOfMembersOfArithmeticProgression(firstTerm, difference, term - 1) + getMemberOfArithmeticProgression(firstTerm, difference, term);
        }
    }
}
