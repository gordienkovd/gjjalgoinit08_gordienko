package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.printMatrix2;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh12N234 {
    public static void main(String[] args) {
        println("Enter height of matrix");
        int height = getIntNumber();
        println("Enter width of matrix");
        int width = getIntNumber();
        println("Enter number of row which will be deleted");
        int numRow = getIntNumber();
        println("Enter number of column which will be deleted");
        int numColumn = getIntNumber();
        int[][] matrix = new int[height][width];
        for (int i = 0, count = 1; i < height; i++) {
            for (int j = 0; j < width; j++) {
                matrix[i][j] = count;
                count++;
            }
        }
        printMatrix2(matrix, height, width);
        println("");
        printMatrix2(deleteRow(matrix, height, width, numRow), height, width);
        println("");
        printMatrix2(deleteColumn(matrix, height, width, numColumn), height, width);
    }

    static int[][] deleteRow(int[][] matrix, int height, int width, int numRow) {
        for (int i = numRow - 1; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (i == height - 1) {
                    matrix[i][j] = 0;
                } else {
                    matrix[i][j] = matrix[i + 1][j];
                }
            }
        }
        return matrix;
    }

    static int[][] deleteColumn(int[][] matrix, int height, int width, int numColumn) {
        for (int i = 0; i < height; i++) {
            for (int j = numColumn - 1; j < width; j++) {
                if (j == width - 1) {
                    matrix[i][j] = 0;
                } else {
                    matrix[i][j] = matrix[i][j + 1];
                }
            }
        }
        return matrix;
    }
}