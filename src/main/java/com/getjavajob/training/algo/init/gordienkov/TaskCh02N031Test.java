package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh02N031.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        testGetInitialNumber();
    }

    private static void testGetInitialNumber() {
        assertEquals("TaskCh02N031Test.testGetInitialNumber", 132, getInitialNumber(123));
    }
}
