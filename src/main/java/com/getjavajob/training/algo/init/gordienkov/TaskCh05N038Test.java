package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh05N038.getDistance;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh05N038.getDistanceToTheHouse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N038Test {
    public static void main(String[] args) {
        testGetDistance();
        testGetDistanceToTheHouse();
    }

    private static void testGetDistance() {
        assertEquals("TaskCh05N038Test.testGetDistance", 1.5, getDistance(2));
    }

    private static void testGetDistanceToTheHouse() {
        assertEquals("TaskCh05N038Test.testGetDistanceToTheHouse", 0.5, getDistanceToTheHouse(2));
    }
}
