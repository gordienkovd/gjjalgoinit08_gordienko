package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh02N043.getRemainderOfDivision;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N043Test {
    public static void main(String[] args) {
        testGetRemainderOfDivisionOne();
        testGetRemainderOfDivisionAnotherNumber();
    }

    private static void testGetRemainderOfDivisionOne() {
        assertEquals("TaskCh02N043Test.testGetRemainderOfDivisionOne", 1, getRemainderOfDivision(1, 1));
    }

    private static void testGetRemainderOfDivisionAnotherNumber() {
        assertEquals("TaskCh02N043Test.testGetRemainderOfDivisionAnotherNumber", 3, getRemainderOfDivision(3, 2));
    }
}
