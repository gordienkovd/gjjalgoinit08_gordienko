package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readDouble;
import static java.lang.Math.*;

public class TaskCh01N017 {
    public static void main(String[] args) {
        println("Enter a real number for method getO");
        double x = readDouble();
        println("Enter a real number for method getP");
        double a = readDouble();
        println("Enter a real number for method getR");
        double b = readDouble();
        println("Enter a real number for method getS");
        double c = readDouble();
        println(getO(x));
        println(getP(a, b, c, x));
        println(getR(x));
        println(getS(x));
    }

    private static double getO(double x) {
        return sqrt(1 - pow(sin(x), 2.0));
    }

    private static double getP(double a, double b, double c, double x) {
        return 1 / sqrt(a * pow(x, 2.00) + b * x + c);
    }

    private static double getR(double x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    private static double getS(double x) {
        return abs(x) + abs(x + 1);
    }
}
