package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh11N158.removeDuplicates;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {
    public static void main(String[] args) {
        testRemoveDuplicates();
    }

    private static void testRemoveDuplicates() {
        assertEquals("TaskCh11N158Test.testRemoveDuplicates", new int[]{1, 2, 3, 0, 0, 0}, removeDuplicates(new int[]{1, 2, 3, 1, 2, 3}));
    }
}
