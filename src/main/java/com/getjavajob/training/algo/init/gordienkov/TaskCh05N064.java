package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh05N064 {
    public static void main(String[] args) {
        District[] districts = new District[12];
        districts[0] = new District(100, 1000);
        districts[1] = new District(200, 2000);
        districts[2] = new District(300, 3000);
        districts[3] = new District(400, 4000);
        districts[4] = new District(500, 5000);
        districts[5] = new District(600, 6000);
        districts[6] = new District(700, 7000);
        districts[7] = new District(800, 8000);
        districts[8] = new District(900, 9000);
        districts[9] = new District(1000, 10000);
        districts[10] = new District(1100, 11000);
        districts[11] = new District(1200, 12000);
        println("The average population density = " + getAverageDensity(districts) + " km²/human");
    }

    static int getAverageDensity(District[] districts) {
        int exprDensity = 0;
        for (District district : districts) {
            exprDensity += district.getArea() / district.getPopulation();
        }
        return exprDensity / 12;
    }
}

class District {
    private int population;
    private int area;

    public District(int population, int area) {
        this.population = population;
        this.area = area;
    }

    public int getPopulation() {
        return population;
    }

    public int getArea() {
        return area;
    }
}