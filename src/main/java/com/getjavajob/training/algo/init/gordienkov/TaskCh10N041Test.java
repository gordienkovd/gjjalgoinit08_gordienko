package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N041.getFactorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N041Test {
    public static void main(String[] args) {
        testGetFactorial();
    }

    private static void testGetFactorial() {
        assertEquals("TaskCh10N041Test.testGetFactorial", 6, getFactorial(3));
    }
}
