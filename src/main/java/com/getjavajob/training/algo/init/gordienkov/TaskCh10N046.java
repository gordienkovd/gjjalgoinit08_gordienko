package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N046 {
    public static void main(String[] args) {
        println("Enter the first term of an geometric progression");
        int firstTerm = getIntNumber();
        println("Enter difference of an geometric progression");
        int denominator = getIntNumber();
        println("Enter the term of an geometric progression");
        int term = getIntNumber();
        println(getMemberOfGeomPro(firstTerm, denominator, term));
        println(getSumOfMembersOGeomPro(firstTerm, denominator, term));
    }

    static int getMemberOfGeomPro(int firstTerm, int denominator, int term) {
        if (term == 1) {
            return firstTerm;
        } else {
            return getMemberOfGeomPro(firstTerm * denominator, denominator, term - 1);
        }
    }

    static int getSumOfMembersOGeomPro(int firstTerm, int denominator, int term) {
        if (term == 1) {
            return firstTerm;
        } else {
            return getSumOfMembersOGeomPro(firstTerm, denominator, term - 1) + getMemberOfGeomPro(firstTerm, denominator, term);
        }
    }
}
