package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N056.isPrimeNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {
    public static void main(String[] args) {
        testIsPrimeNumberTrue();
        testIsPrimeNumberFalse();
    }

    private static void testIsPrimeNumberTrue() {
        assertEquals("TaskCh10N056Test.testIsPrimeNumberTrue", true, isPrimeNumber(7, 2));
    }

    private static void testIsPrimeNumberFalse() {
        assertEquals("TaskCh10N056Test.testIsPrimeNumberFalse", false, isPrimeNumber(10, 10));
    }
}
