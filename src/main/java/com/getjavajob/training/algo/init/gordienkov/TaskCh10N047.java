package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N047 {
    public static void main(String[] args) {
        println("Enter a member of the Fibonacci sequence");
        int term = getIntNumber();
        println(getMemberOfFibSeq(term));
    }

    static int getMemberOfFibSeq(int term) {
        if (term == 1 || term == 2) {
            return 1;
        } else {
            return getMemberOfFibSeq(term - 1) + getMemberOfFibSeq(term - 2);
        }
    }
}
