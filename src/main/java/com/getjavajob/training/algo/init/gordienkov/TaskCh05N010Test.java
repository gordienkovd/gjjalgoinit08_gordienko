package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh05N010.getDollarCourse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N010Test {
    public static void main(String[] args) {
        testGetDollarCourse();
    }

    private static void testGetDollarCourse() {
        double[] expected = new double[]{
                2.00, 4.00, 6.00, 8.00,
                10.00, 12.00, 14.00, 16.00,
                18.00, 20.00, 22.00, 24.00,
                26.00, 28.00, 30.00, 32.00,
                34.00, 36.00, 38.00, 40.00
        };
        double[] exprActual = new double[]{
                1.00, 2.00, 3.00, 4.00,
                5.00, 6.00, 7.00, 8.00,
                9.00, 10.00, 11.00, 12.00,
                13.00, 14.00, 15.00, 16.00,
                17.00, 18.00, 19.00, 20.00
        };
        assertEquals("TaskCh05N010Test.testGetDollarCourse", expected, getDollarCourse(exprActual, 2));
    }
}
