package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.printMatrix2;

public class TaskCh12N028 {
    public static void main(String[] args) {
        printMatrix2(getCircularMatrix(new int[5][5]), 5, 5);
    }

    static int[][] getCircularMatrix(int[][] matrix) {
        int count = 1;
        int columnStart = 0;
        int columnEnd = matrix.length - 1;
        int rowStart = 0;
        int rowEnd = matrix.length - 1;
        while (count <= matrix.length * matrix.length) {
            for (int i = columnStart; i <= columnEnd; i++) {
                matrix[rowStart][i] = count++;
            }
            for (int j = rowStart + 1; j <= rowEnd; j++) {
                matrix[j][columnEnd] = count++;
            }
            for (int i = columnEnd - 1; i >= columnStart; i--) {
                matrix[rowEnd][i] = count++;
            }
            for (int j = rowEnd - 1; j >= rowStart + 1; j--) {
                matrix[j][columnStart] = count++;
            }
            columnStart++;
            columnEnd--;
            rowStart++;
            rowEnd--;
        }
        return matrix;
    }
}
