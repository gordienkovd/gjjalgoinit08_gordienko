package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N052 {
    public static void main(String[] args) {
        println("Enter natural number");
        int num = getIntNumber();
        println(getReverseNumber(num, 0));
    }

    static int getReverseNumber(int number, int result) {
        if (number == 0) {
            return result;
        } else {
            result= result * 10 + number % 10;
            return getReverseNumber(number / 10, result);
        }
    }
}
