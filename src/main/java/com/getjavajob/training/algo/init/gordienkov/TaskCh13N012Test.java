package com.getjavajob.training.algo.init.gordienkov;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh13N012Test {
    public static void main(String[] args) {
        testGetNumOfYearsWorked();
        testSearchEmployeesBySubstring();
        testSearchEmployeesByYear();
    }

    private static void testGetNumOfYearsWorked() {
        Employee testEmployee = new Employee("Ivan", "Ivanovich", "Ivanov", "Moscow", 3, 2011);
        assertEquals("TaskCh13N012Test.testGetNumOfYearsWorked", 5, testEmployee.getNumOfYearsWorked(7, 2016));
    }

    private static void testSearchEmployeesBySubstring() {
        Database data = new Database();
        data.addEmployees(new Employee("Ivan", "Ivanovich", "Ivanov", "Moscow", 3, 2011));
        data.addEmployees(new Employee("Petr", "Antonovich", "Ivanov", "Moscow", 1, 2008));
        data.addEmployees(new Employee("Sidor", "Petrovich", "Antonov", "Moscow", 2, 2015));
        data.addEmployees(new Employee("Anton", "Gennadievich", "Vasilev", "Moscow", 4, 2001));
        data.addEmployees(new Employee("James", "Bond", "Boston", 1, 1985));
        data.addEmployees(new Employee("Tom", "Bond", "New York", 5, 2016));
        data.addEmployees(new Employee("Eduard", "Sergeevich", "Ivanenko", "Boston", 11, 2014));
        data.addEmployees(new Employee("Sergey", "Sergeevich", "Sergeev", "Samara", 10, 2013));
        data.addEmployees(new Employee("Aleksey", "Gennadevich", "Vasilev", "Lobnya", 9, 2012));
        data.addEmployees(new Employee("Petr", "Sergeevich", "Ivanenko", "Moscow", 8, 2011));
        data.addEmployees(new Employee("Andrey", "Andreevich", "Andreev", "Moscow", 7, 2010));
        data.addEmployees(new Employee("Andrey", "Petrovich", "Sergeenko", "Moscow", 6, 2009));
        data.addEmployees(new Employee("Petr", "Petrovich", "Sergeenko", "Moscow", 5, 2008));
        data.addEmployees(new Employee("Andrey", "Alekseevich", "Petrenko", "Moscow", 4, 2007));
        data.addEmployees(new Employee("Fedor", "Fedorovich", "Fedorov", "Moscow", 3, 2006));
        data.addEmployees(new Employee("Andrey", "Alekseevich", "Petrenko", "Moscow", 2, 2005));
        data.addEmployees(new Employee("Grigoriy", "Alekseevich", "Mihailov", "Moscow", 1, 2004));
        data.addEmployees(new Employee("Vasiliy", "Fedorovich", "Svetlov", "Moscow", 12, 2003));
        data.addEmployees(new Employee("Anna", "Andreevna", "Ivanova", "Moscow", 11, 2002));
        data.addEmployees(new Employee("Svetlana", "Sergeevna", "Gryzlova", "Moscow", 10, 2001));
        List<Employee> testEmployees1 = new ArrayList<>();
        testEmployees1.add(new Employee("Anna", "Andreevna", "Ivanova", "Moscow", 11, 2002));
        assertEquals("TaskCh13N012Test.testSearchEmployeesBySubstring", testEmployees1, data.searchEmployeesBySubstring("anna"));
    }

    private static void testSearchEmployeesByYear() {
        Database data = new Database();
        data.addEmployees(new Employee("Ivan", "Ivanovich", "Ivanov", "Moscow", 3, 2011));
        data.addEmployees(new Employee("Petr", "Antonovich", "Ivanov", "Moscow", 1, 2008));
        data.addEmployees(new Employee("Sidor", "Petrovich", "Antonov", "Moscow", 2, 2015));
        data.addEmployees(new Employee("Anton", "Gennadievich", "Vasilev", "Moscow", 4, 2001));
        data.addEmployees(new Employee("James", "Bond", "Boston", 1, 1985));
        data.addEmployees(new Employee("Tom", "Bond", "New York", 5, 2016));
        data.addEmployees(new Employee("Eduard", "Sergeevich", "Ivanenko", "Boston", 11, 2014));
        data.addEmployees(new Employee("Sergey", "Sergeevich", "Sergeev", "Samara", 10, 2013));
        data.addEmployees(new Employee("Aleksey", "Gennadevich", "Vasilev", "Lobnya", 9, 2012));
        data.addEmployees(new Employee("Petr", "Sergeevich", "Ivanenko", "Moscow", 8, 2011));
        data.addEmployees(new Employee("Andrey", "Andreevich", "Andreev", "Moscow", 7, 2010));
        data.addEmployees(new Employee("Andrey", "Petrovich", "Sergeenko", "Moscow", 6, 2009));
        data.addEmployees(new Employee("Petr", "Petrovich", "Sergeenko", "Moscow", 5, 2008));
        data.addEmployees(new Employee("Andrey", "Alekseevich", "Petrenko", "Moscow", 4, 2007));
        data.addEmployees(new Employee("Fedor", "Fedorovich", "Fedorov", "Moscow", 3, 2006));
        data.addEmployees(new Employee("Andrey", "Alekseevich", "Petrenko", "Moscow", 2, 2005));
        data.addEmployees(new Employee("Grigoriy", "Alekseevich", "Mihailov", "Moscow", 1, 2004));
        data.addEmployees(new Employee("Vasiliy", "Fedorovich", "Svetlov", "Moscow", 12, 2003));
        data.addEmployees(new Employee("Anna", "Andreevna", "Ivanova", "Moscow", 11, 2002));
        data.addEmployees(new Employee("Svetlana", "Sergeevna", "Gryzlova", "Moscow", 10, 2001));
        List<Employee> testEmployees2 = new ArrayList<>();
        testEmployees2.add(new Employee("James", "Bond", "Boston", 1, 1985));
        assertEquals("TaskCh13N012Test.testSearchEmployeesByYear", testEmployees2, data.searchEmployeesByYear(7, 2016, 20));
    }
}
