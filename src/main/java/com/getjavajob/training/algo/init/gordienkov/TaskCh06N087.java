package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.*;

public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

class Game {
    private String nameOfTeam1;
    private String nameOfTeam2;
    private int scoreOfTeam1;
    private int scoreOfTeam2;
    private boolean isGameOver;

    public void setNameOfTeam1(String nameOfTeam1) {
        this.nameOfTeam1 = nameOfTeam1;
    }

    public void setNameOfTeam2(String nameOfTeam2) {
        this.nameOfTeam2 = nameOfTeam2;
    }

    public void setGameOver(boolean gameOver) {
        isGameOver = gameOver;
    }

    public void setScoreOfTeam1(int scoreOfTeam1) {
        this.scoreOfTeam1 = scoreOfTeam1;
    }

    public void setScoreOfTeam2(int scoreOfTeam2) {
        this.scoreOfTeam2 = scoreOfTeam2;
    }

    public void play() {
        print("Enter team #1:");
        nameOfTeam1 = readLine();
        print("Enter team #2:");
        nameOfTeam2 = readLine();
        isGameOver = false;
        while (!isGameOver) {
            int point;
            print("Enter team to score (1 or 2 or 0 to finish game): ");
            int team = getIntNumberInRange2(-1, 3);
            if (team == 0) {
                isGameOver = true;
                println(result());
            } else if (team == 1) {
                print("Enter score (1 or 2 or 3)): ");
                point = getIntNumberInRange2(0, 4);
                scoreOfTeam1 += point;
                println(score());
            } else if (team == 2) {
                print("Enter score (1 or 2 or 3)): ");
                point = getIntNumberInRange2(0, 4);
                scoreOfTeam2 += point;
                println(score());
            }
        }
    }

    String score() {
        return nameOfTeam1 + " " + scoreOfTeam1 + " : " + scoreOfTeam2 + " " + nameOfTeam2;
    }

    String result() {
        String outputString = nameOfTeam1 + " " + scoreOfTeam1 + " : " + scoreOfTeam2 + " " + nameOfTeam2;
        if (scoreOfTeam1 > scoreOfTeam2) {
            return outputString + " \nWinner: " + nameOfTeam1;
        } else if (scoreOfTeam2 > scoreOfTeam1) {
            return outputString + " \nWinner: " + nameOfTeam2;
        } else {
            return outputString + " \nDraw";
        }
    }
}
