package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readLine;

public class TaskCh09N107 {
    public static void main(String[] args) {
        println("Enter Word");
        String word = readLine();
        println(replaceLetters(word));
    }

    static String replaceLetters(String word) {
        if (word.contains("a") & word.contains("o")) {
            char[] chars = word.toCharArray();
            String result = "";
            for (int i = chars.length - 1; i != 0; i--) {
                if (chars[i] == 'o') {
                    chars[i] = 'a';
                    break;
                }
            }
            for (char letter : chars) {
                result += letter;
            }
            return result.replaceFirst("a", "o");
        } else {
            return "The letters 'a' and 'o' missing in the word";
        }
    }
}
