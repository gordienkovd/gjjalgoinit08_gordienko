package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh11N245.sortNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {
    public static void main(String[] args) {
        testSortNumbers();
    }

    private static void testSortNumbers() {
        assertEquals("TaskCh11N245Test.testSortNumbers", new int[]{-1, -2, -3, 1, 2, 3}, sortNumbers(new int[]{-1, 1, 2, -2, -3, 3}));
    }
}
