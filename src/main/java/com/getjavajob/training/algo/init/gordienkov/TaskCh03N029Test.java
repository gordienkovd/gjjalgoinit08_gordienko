package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh03N029.*;

public class TaskCh03N029Test {
    public static void main(String[] args) {
        testGetVariantATrue();
        testGetVariantAFalse();
        testGetVariantBTrue();
        testGetVariantBFalse();
        testGetVariantVTrue();
        testGetVariantVFalse();
        testGetVariantGTrue();
        testGetVariantGFalse();
        testGetVariantDTrueFirstNumber();
        testGetVariantDFalseFirstNumber();
        testGetVariantDTrueSecondNumber();
        testGetVariantDFalseSecondNumber();
        testGetVariantDTrueThirdNumber();
        testGetVariantDFalseThirdNumber();
        testGetVariantETrue();
        testGetVariantEFalse();
    }

    private static void testGetVariantATrue() {
        assertEquals("TaskCh03N029Test.testGetVariantATrue", true, getVariantA(11, 5));
    }

    private static void testGetVariantAFalse() {
        assertEquals("TaskCh03N029Test.testGetVariantAFalse", false, getVariantA(2, 5));
    }

    private static void testGetVariantBTrue() {
        assertEquals("TaskCh03N029Test.testGetVariantBTrue", true, getVariantB(10, 21));
    }

    private static void testGetVariantBFalse() {
        assertEquals("TaskCh03N029Test.testGetVariantBFalse", false, getVariantB(12, 11));
    }

    private static void testGetVariantVTrue() {
        assertEquals("TaskCh03N029Test.testGetVariantVTrue", true, getVariantV(0, 10));
    }

    private static void testGetVariantVFalse() {
        assertEquals("TaskCh03N029Test.testGetVariantVFalse", false, getVariantV(5, 10));
    }

    private static void testGetVariantGTrue() {
        assertEquals("TaskCh03N029Test.testGetVariantGTrue", true, getVariantG(-1, -2, -3));
    }

    private static void testGetVariantGFalse() {
        assertEquals("TaskCh03N029Test.testGetVariantGFalse", false, getVariantG(1, -2, -3));
    }

    private static void testGetVariantDTrueFirstNumber() {
        assertEquals("TaskCh03N029Test.testGetVariantDTrueFirstNumber", true, getVariantD(5, 1, 2));
    }

    private static void testGetVariantDFalseFirstNumber() {
        assertEquals("TaskCh03N029Test.testGetVariantDFalseFirstNumber", false, getVariantD(5, 10, 15));
    }

    private static void testGetVariantDTrueSecondNumber() {
        assertEquals("TaskCh03N029Test.testGetVariantDTrueSecondNumber", true, getVariantD(1, 5, 2));
    }

    private static void testGetVariantDFalseSecondNumber() {
        assertEquals("TaskCh03N029Test.testGetVariantDFalseSecondNumber", false, getVariantD(1, 5, 15));
    }

    private static void testGetVariantDTrueThirdNumber() {
        assertEquals("TaskCh03N029Test.testGetVariantDTrueThirdNumber", true, getVariantD(1, 2, 5));
    }

    private static void testGetVariantDFalseThirdNumber() {
        assertEquals("TaskCh03N029Test.testGetVariantDFalseThirdNumber", false, getVariantD(5, 1, 15));
    }

    private static void testGetVariantETrue() {
        assertEquals("TaskCh03N029Test.testGetVariantETrue", true, getVariantE(101, 1, 2));
    }

    private static void testGetVariantEFalse() {
        assertEquals("TaskCh03N029Test.testGetVariantEFalse", false, getVariantE(0, 1, 2));
    }
}
