package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N046.getMemberOfGeomPro;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N046.getSumOfMembersOGeomPro;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {
    public static void main(String[] args) {
        testGetMemberOfGeomPro();
        testGetSumOfMembersOGeomPro();
    }

    private static void testGetMemberOfGeomPro() {
        assertEquals("TaskCh10N046Test.testGetMemberOfGeomPro", 18, getMemberOfGeomPro(2, 3, 3));
    }

    private static void testGetSumOfMembersOGeomPro() {
        assertEquals("TaskCh10N046Test.testGetSumOfMembersOGeomPro", 26, getSumOfMembersOGeomPro(2, 3, 3));
    }
}