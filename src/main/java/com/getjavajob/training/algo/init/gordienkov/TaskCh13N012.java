package com.getjavajob.training.algo.init.gordienkov;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;

class TaskCh13N012 {
    public static void main(String[] args) {
        Database data = new Database();
        data.addEmployees(new Employee("Ivan", "Ivanovich", "Ivanov", "Moscow", 3, 2011));
        data.addEmployees(new Employee("Petr", "Antonovich", "Ivanov", "Moscow", 1, 2008));
        data.addEmployees(new Employee("Sidor", "Petrovich", "Antonov", "Moscow", 2, 2015));
        data.addEmployees(new Employee("Anton", "Gennadievich", "Vasilev", "Moscow", 4, 2001));
        data.addEmployees(new Employee("James", "Bond", "Boston", 1, 1985));
        data.addEmployees(new Employee("Tom", "Bond", "New York", 5, 2016));
        data.addEmployees(new Employee("Eduard", "Sergeevich", "Ivanenko", "Boston", 11, 2014));
        data.addEmployees(new Employee("Sergey", "Sergeevich", "Sergeev", "Samara", 10, 2013));
        data.addEmployees(new Employee("Aleksey", "Gennadevich", "Vasilev", "Lobnya", 9, 2012));
        data.addEmployees(new Employee("Petr", "Sergeevich", "Ivanenko", "Moscow", 8, 2011));
        data.addEmployees(new Employee("Andrey", "Andreevich", "Andreev", "Moscow", 7, 2010));
        data.addEmployees(new Employee("Andrey", "Petrovich", "Sergeenko", "Moscow", 6, 2009));
        data.addEmployees(new Employee("Petr", "Petrovich", "Sergeenko", "Moscow", 5, 2008));
        data.addEmployees(new Employee("Andrey", "Alekseevich", "Petrenko", "Moscow", 4, 2007));
        data.addEmployees(new Employee("Fedor", "Fedorovich", "Fedorov", "Moscow", 3, 2006));
        data.addEmployees(new Employee("Andrey", "Alekseevich", "Petrenko", "Moscow", 2, 2005));
        data.addEmployees(new Employee("Grigoriy", "Alekseevich", "Mihailov", "Moscow", 1, 2004));
        data.addEmployees(new Employee("Vasiliy", "Fedorovich", "Svetlov", "Moscow", 12, 2003));
        data.addEmployees(new Employee("Anna", "Andreevna", "Ivanova", "Moscow", 11, 2002));
        data.addEmployees(new Employee("Svetlana", "Sergeevna", "Gryzlova", "Moscow", 10, 2001));
        List<Employee> employeesByYear = data.searchEmployeesByYear(7, 2016, 20);
        data.printEmployees(employeesByYear);
        println("");
        List<Employee> employeesBySubstring = data.searchEmployeesBySubstring("anna");
        data.printEmployees(employeesBySubstring);
    }
}

class Employee {
    private String name;
    private String middleName;
    private String surname;
    private String address;
    private int monthOfEmployment;
    private int yearOfEmployment;

    public Employee(String name, String middleName, String surname, String address, int monthOfEmployment, int yearOfEmployment) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.monthOfEmployment = monthOfEmployment;
        this.yearOfEmployment = yearOfEmployment;
        this.middleName = middleName;
    }

    public Employee(String name, String surname, String address, int monthOfEmployment, int yearOfEmployment) {
        this(name, "", surname, address, monthOfEmployment, yearOfEmployment);
    }

    String getName() {
        return name;
    }

    String getMiddleName() {
        return middleName;
    }

    String getSurname() {
        return surname;
    }

    int getNumOfYearsWorked(int month, int year) {
        int exprMonths = year * 12 + month;
        int exprMonthsWorked = yearOfEmployment * 12 + monthOfEmployment;
        return (exprMonths - exprMonthsWorked) / 12;
    }

    @Override
    public String toString() {
        if (!Objects.equals(middleName, "")) {
            return name +
                    ", " +
                    middleName +
                    ", " +
                    surname +
                    ", " +
                    address +
                    ", " +
                    monthOfEmployment +
                    ", " +
                    yearOfEmployment;
        } else {
            return name +
                    ", " +
                    surname +
                    ", " +
                    address +
                    ", " +
                    monthOfEmployment +
                    ", " +
                    yearOfEmployment;
        }
    }
}

class Database {
    private List<Employee> employees;

    public Database() {
        employees = new ArrayList<>();
    }

    void addEmployees(Employee employee) {
        employees.add(employee);
    }

    List<Employee> searchEmployeesBySubstring(String substring) {
        List<Employee> exprEmployees = new ArrayList<>();
        for (Employee employee : employees) {
            String name = employee.getName().toLowerCase();
            String surname = employee.getSurname().toLowerCase();
            if (!Objects.equals(employee.getMiddleName(), "")) {
                String middleName = employee.getMiddleName().toLowerCase();
                if (name.contains(substring.toLowerCase()) || middleName.contains(substring.toLowerCase()) || surname.contains(substring.toLowerCase())) {
                    exprEmployees.add(employee);
                }
            } else {
                if (name.contains(substring.toLowerCase()) || surname.contains(substring.toLowerCase())) {
                    exprEmployees.add(employee);
                }
            }
        }
        return exprEmployees;
    }

    List<Employee> searchEmployeesByYear(int month, int year, int numOfYears) {
        List<Employee> exprEmployees = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getNumOfYearsWorked(month, year) >= numOfYears) {
                exprEmployees.add(employee);
            }
        }
        return exprEmployees;
    }

    void printEmployees(List<Employee> employees) {
        for (Employee employee : employees) {
            println(employee.toString());
        }
    }
}