package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh03N029 {
    public static void main(String[] args) {
        println("Variant a): each of the numbers X and Y is odd - " + getVariantA(11, 5));
        println("Variant b): only one of the numbers X and Y less than 20 - " + getVariantB(10, 21));
        println("Variant v): at least one of the numbers X and Y is equal to zero - " + getVariantV(0, 10));
        println("Variant g): each of the numbers X, Y, Z negative - " + getVariantG(-1, -2, -3));
        println("Variant d): only one of the numbers X, Y and Z are multiples of five - " + getVariantD(5, 1, 2));
        println("Variant e): at least one of the numbers X, Y, Z of more than 100 - " + getVariantE(90, 101, 101));
    }

    /**
     * @param x - first number
     * @param y - second number
     * @return - true if each of the numbers X and Y is odd.
     */
    static boolean getVariantA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * @param x - first number
     * @param y - second number
     * @return - true if only one of the numbers X and Y less than 20.
     */
    static boolean getVariantB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * @param x - first number
     * @param y - second number
     * @return - true if at least one of the numbers X and Y is equal to zero.
     */

    static boolean getVariantV(int x, int y) {
        return x == 0 ^ y == 0;
    }

    /**
     * @param x - first number
     * @param y - second number
     * @param z - third number
     * @return - true if each of the numbers X, Y, Z negative.
     */

    static boolean getVariantG(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * @param x - first number
     * @param y - second number
     * @param z - third number
     * @return - true if only one of the numbers X, Y and Z are multiples of five.
     */

    static boolean getVariantD(int x, int y, int z) {
        boolean x5 = x % 5 == 0;
        boolean y5 = y % 5 == 0;
        boolean z5 = z % 5 == 0;
        return (x5 ^ y5 ^ z5) ^ (x5 && y5 && z5);
    }

    /**
     * @param x - first number
     * @param y - second number
     * @param z - third number
     * @return - true if at least one of the numbers X, Y, Z of more than 100.
     */

    static boolean getVariantE(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
