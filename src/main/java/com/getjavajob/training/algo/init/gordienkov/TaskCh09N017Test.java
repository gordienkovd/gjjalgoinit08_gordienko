package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh09N017.compareLetters;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh09N017Test {
    public static void main(String[] args) {
        compareLettersSame();
        compareLettersDifferent();
    }

    private static void compareLettersSame() {
        assertEquals("TaskCh09N017Test.compareLettersSame", true, compareLetters("test"));
    }

    private static void compareLettersDifferent() {
        assertEquals("TaskCh09N017Test.compareLettersDifferent", false, compareLetters("java"));
    }
}
