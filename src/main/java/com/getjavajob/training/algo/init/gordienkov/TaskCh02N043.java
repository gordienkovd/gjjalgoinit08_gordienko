package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readLine;

public class TaskCh02N043 {
    public static void main(String[] args) {
        println("Enter first number");
        int firstNumber = Integer.parseInt(readLine());
        println("Enter second number");
        int secondNumber = Integer.parseInt(readLine());
        println(getRemainderOfDivision(firstNumber, secondNumber));
    }

    static int getRemainderOfDivision(int firstNumber, int secondNumber) {
        return (firstNumber % secondNumber) * (secondNumber % firstNumber) + 1;
    }
}
