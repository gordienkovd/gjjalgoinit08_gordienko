package com.getjavajob.training.algo.init.gordienkov;


import static com.getjavajob.training.algo.init.gordienkov.TaskCh04N106.getSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N106Test {
    public static void main(String[] args) {
        testGetSeasonWinterDecember();
        testGetSeasonWinterJanuary();
        testGetSeasonWinterFebruary();
        testGetSeasonSpringMarch();
        testGetSeasonSpringApril();
        testGetSeasonSpringMay();
        testGetSeasonSummerJune();
        testGetSeasonSummerJule();
        testGetSeasonSummerAugust();
        testGetSeasonAutumnSeptember();
        testGetSeasonAutumnOctober();
        testGetSeasonAutumnNovember();
    }

    private static void testGetSeasonWinterDecember() {
        assertEquals("TaskCh04N106Test.testGetSeasonWinterDecember", "Winter", getSeason(12));
    }

    private static void testGetSeasonWinterJanuary() {
        assertEquals("TaskCh04N106Test.testGetSeasonWinterJanuary", "Winter", getSeason(1));
    }

    private static void testGetSeasonWinterFebruary() {
        assertEquals("TaskCh04N106Test.testGetSeasonWinterFebruary", "Winter", getSeason(2));
    }

    private static void testGetSeasonSpringMarch() {
        assertEquals("TaskCh04N106Test.testGetSeasonSpringMarch", "Spring", getSeason(3));
    }

    private static void testGetSeasonSpringApril() {
        assertEquals("TaskCh04N106Test.testGetSeasonSpringMarch", "Spring", getSeason(4));
    }

    private static void testGetSeasonSpringMay() {
        assertEquals("TaskCh04N106Test.testGetSeasonSpringMay", "Spring", getSeason(5));
    }

    private static void testGetSeasonSummerJune() {
        assertEquals("TaskCh04N106Test.testGetSeasonSummerJune", "Summer", getSeason(6));
    }

    private static void testGetSeasonSummerJule() {
        assertEquals("TaskCh04N106Test.testGetSeasonSummerJule", "Summer", getSeason(7));
    }

    private static void testGetSeasonSummerAugust() {
        assertEquals("TaskCh04N106Test.testGetSeasonSummerAugust", "Summer", getSeason(8));
    }

    private static void testGetSeasonAutumnSeptember() {
        assertEquals("TaskCh04N106Test.testGetSeasonAutumnSeptember", "Autumn", getSeason(9));
    }

    private static void testGetSeasonAutumnOctober() {
        assertEquals("TaskCh04N106Test.testGetSeasonAutumnOctober", "Autumn", getSeason(10));
    }

    private static void testGetSeasonAutumnNovember() {
        assertEquals("TaskCh04N106Test.testGetSeasonAutumnNovember", "Autumn", getSeason(11));
    }
}
