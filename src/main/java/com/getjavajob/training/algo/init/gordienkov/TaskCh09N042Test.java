package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh09N042.reverseWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N042Test {
    public static void main(String[] args) {
        testReverseWord();
    }

    private static void testReverseWord() {
        assertEquals("TaskCh09N042Test.testReverseWord", "tset", reverseWord("test"));
    }
}
