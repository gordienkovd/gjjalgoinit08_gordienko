package com.getjavajob.training.algo.init.gordienkov;

import java.util.Arrays;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh11N158 {
    public static void main(String[] args) {
        int[] numbers = new int[]{1, 2, 3, 4, 4, 7, 7, 8, 9, 10, 2, 3, 1, 5, 4, 7, 2, 1};
        println(Arrays.toString(removeDuplicates(numbers)));
    }

    static int[] removeDuplicates(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length; j++) {
                if (i != j) {
                    if (numbers[i] == numbers[j]) {
                        for (int k = j; k < numbers.length; k++) {
                            if (k != numbers.length - 1) {
                                numbers[k] = numbers[k + 1];
                            } else {
                                numbers[k] = 0;
                            }
                        }
                    }
                }
            }
        }
        return numbers;
    }
}
