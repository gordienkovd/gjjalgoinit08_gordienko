package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readLine;

public class TaskCh09N022 {
    public static void main(String[] args) {
        println("Type a word with an even number of letters");
        String word = readLine();
        println(getHalfOfTheWord(word));
    }

    static String getHalfOfTheWord(String word) {
        return word.substring(0, word.length() / 2);
    }
}
