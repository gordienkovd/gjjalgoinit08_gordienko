package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readDouble;

public class TaskCh04N036 {
    public static void main(String[] args) {
        println("Enter minutes");
        println(knowColorOfTrafficLight(readDouble()));
    }

    static String knowColorOfTrafficLight(double minutes) {
        double minutesInOneRotation = 5;
        String colorOfTrafficLight = "Green";
        if (minutes % minutesInOneRotation >= 3.0) {
            colorOfTrafficLight = "Red";
        }
        return colorOfTrafficLight;
    }
}
