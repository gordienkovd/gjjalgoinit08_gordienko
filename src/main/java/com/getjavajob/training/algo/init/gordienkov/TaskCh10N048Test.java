package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N048.maxValueOfArr;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {
    public static void main(String[] args) {
        testMaxValueOfArr();
    }

    private static void testMaxValueOfArr() {
        assertEquals("TaskCh10N048Test.testMaxValueOfArr", -1, maxValueOfArr(new int[]{-4, -1, -6, -2}, 0, -1));
    }
}
