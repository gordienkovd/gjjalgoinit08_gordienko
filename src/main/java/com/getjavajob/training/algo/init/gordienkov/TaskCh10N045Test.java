package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N045.getMemberOfArithmeticProgression;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N045.getSumOfMembersOfArithmeticProgression;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N045Test {
    public static void main(String[] args) {
        testGetMemberOfArithmeticProgression();
        testGetSumOfMembersOfArithmeticProgression();
    }

    private static void testGetMemberOfArithmeticProgression() {
        assertEquals("TaskCh10N045Test.testGetMemberOfArithmeticProgression", 8, getMemberOfArithmeticProgression(2, 3, 3));
    }

    private static void testGetSumOfMembersOfArithmeticProgression() {
        assertEquals("TaskCh10N045Test.testSumOfMembersOfArithmeticProgression", 15, getSumOfMembersOfArithmeticProgression(2, 3, 3));
    }
}
