package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh04N015.getAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        testGetAgeWasBirthDay();
        testGetAgeIsBirthDay();
        testGetAgeNotHadABirthDay();
    }

    private static void testGetAgeWasBirthDay() {
        assertEquals("TaskCh04N015Test.testGetAgeWasBirthDay", 29, getAge(12, 2014, 6, 1985));
    }

    private static void testGetAgeIsBirthDay() {
        assertEquals("TaskCh04N015Test.testGetAgeIsBirthDay", 29, getAge(6, 2014, 6, 1985));
    }

    private static void testGetAgeNotHadABirthDay() {
        assertEquals("TaskCh04N015Test.testGetAgeNotHadABirthDay", 28, getAge(5, 2014, 6, 1985));
    }
}
