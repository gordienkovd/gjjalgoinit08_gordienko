package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readLine;

public class TaskCh09N042 {
    public static void main(String[] args) {
        println("Enter word");
        String word = readLine();
        println(reverseWord(word));
    }

    static String reverseWord(String word) {
        StringBuilder result = new StringBuilder(word).reverse();
        return result.toString();
    }
}
