package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N053.getReverseNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {
    public static void main(String[] args) {
        testGetReverseNumbers();
    }

    private static void testGetReverseNumbers() {
        assertEquals("TaskCh10N053Test.testGetReverseNumbers", new int[]{3, 2, 1}, getReverseNumbers(new int[]{1, 2, 3}, 3, new int[3]));
    }
}
