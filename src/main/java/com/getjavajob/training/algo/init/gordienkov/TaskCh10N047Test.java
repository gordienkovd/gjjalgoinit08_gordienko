package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N047.getMemberOfFibSeq;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N047Test {
    public static void main(String[] args) {
        testMemberOfFibSeq();
    }

    private static void testMemberOfFibSeq() {
        assertEquals("TaskCh10N047Test.testGetMemberOfFibSeq", 8, getMemberOfFibSeq(6));
    }
}
