package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh04N033.isEven;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        testIsEvenTrue();
        testIsEvenFalse();
    }

    private static void testIsEvenTrue() {
        assertEquals("TaskCh04N033Test.testIsEvenTrue", true, isEven(2));
    }

    private static void testIsEvenFalse() {
        assertEquals("TaskCh04N033Test.testIsEvenFalse", false, isEven(1));
    }
}
