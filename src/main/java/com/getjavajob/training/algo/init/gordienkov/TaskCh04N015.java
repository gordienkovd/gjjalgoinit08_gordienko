package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumberInRange;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

class TaskCh04N015 {
    static void main(String[] args) {
        println("Enter the current month");
        int currentMonth = getIntNumberInRange(0, 13);
        println("Enter the current year");
        int currentYear = getIntNumber();
        println("Enter birth month");
        int birthDayMonth = getIntNumberInRange(0, 13);
        println("Enter the birth year");
        int birthDayYear = getIntNumber();
        println(getAge(currentMonth, currentYear, birthDayMonth, birthDayYear));
    }

    static int getAge(int currentMonth, int currentYear, int birthDayMonth, int birthDayYear) {
        int differenceYears = currentYear - birthDayYear;
        if (currentMonth < birthDayMonth) {
            return differenceYears - 1;
        } else {
            return differenceYears;
        }
    }
}
