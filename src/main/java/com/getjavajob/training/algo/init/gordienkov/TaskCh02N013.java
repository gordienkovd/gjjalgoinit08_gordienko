package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumberInRange;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh02N013 {

    public static void main(String[] args) {
        int num = getIntNumberInRange(100, 200);
        println(reverseNum(num));
    }

    static int reverseNum(int num) {
        int thirdNumber = num / 100;
        int secondNumber = (num % 100) / 10;
        int firstNumber = num - thirdNumber * 100 - secondNumber * 10;
        return firstNumber * 100 + secondNumber * 10 + thirdNumber;
    }
}
