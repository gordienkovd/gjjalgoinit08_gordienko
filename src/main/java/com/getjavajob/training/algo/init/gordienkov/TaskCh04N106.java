package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumberInRange;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh04N106 {
    public static void main(String[] args) {
        println("Enter the ordinal number of the month");
        println(getSeason(getIntNumberInRange(0, 13)));
    }

    static String getSeason(int month) {
        switch (month) {
            case 12:
            case 1:
            case 2:
                return  "Winter";
            case 3:
            case 4:
            case 5:
                return "Spring";
            case 6:
            case 7:
            case 8:
                return "Summer";
            case 9:
            case 10:
            case 11:
                return "Autumn";
            default:
                return "Day is incorrect";
        }
    }
}
