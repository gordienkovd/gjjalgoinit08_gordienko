package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh04N067.checkItIsWorkdayOrWeekend;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N067Test {
    public static void main(String[] args) {
        testCheckItIsWorkdayOrWeekendWorkDay();
        testCheckItIsWorkdayOrWeekendWeekend();
    }

    private static void testCheckItIsWorkdayOrWeekendWorkDay() {
        assertEquals("TaskCh04N067Test.testCheckItIsWorkdayOrWeekendWorkDay", "Workday", checkItIsWorkdayOrWeekend(5));
    }

    private static void testCheckItIsWorkdayOrWeekendWeekend() {
        assertEquals("TaskCh04N067Test.testCheckItIsWorkdayOrWeekendWeekend", "Weekend", checkItIsWorkdayOrWeekend(7));
    }
}
