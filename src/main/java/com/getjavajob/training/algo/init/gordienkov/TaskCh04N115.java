package com.getjavajob.training.algo.init.gordienkov;


import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumberFrom;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh04N115 {
    public static void main(String[] args) {
        int year = getIntNumberFrom(1983);
        println(getAnimal(year) + ", " + getColor(year));
    }

    static String getAnimal(int year) {
        int exprYear = (year - 1984) % 12;
        switch (exprYear) {
            case 0:
                return "Rat";
            case 1:
                return "Cow";
            case 2:
                return "Tiger";
            case 3:
                return "Hare";
            case 4:
                return "Dragon";
            case 5:
                return "Snake";
            case 6:
                return "Horse";
            case 7:
                return "Sheep";
            case 8:
                return "Monkey";
            case 9:
                return "Cock";
            case 10:
                return "Dog";
            case 11:
                return "Pig";
            default:
                return "Wrong year inserted";
        }
    }

    static String getColor(int year) {
        int exprYear = (year - 1984) % 10;
        switch (exprYear) {
            case 0:
            case 1:
                return "Green";
            case 2:
            case 3:
                return "Red";
            case 4:
            case 5:
                return "Yellow";
            case 6:
            case 7:
                return "White";
            case 8:
            case 9:
                return "Black";
            default:
                return "Wrong year inserted";
        }
    }
}
