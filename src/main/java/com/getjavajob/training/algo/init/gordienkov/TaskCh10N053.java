package com.getjavajob.training.algo.init.gordienkov;

import java.util.Arrays;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N053 {
    public static void main(String[] args) {
        println("Enter array length");
        int arrayLength = getIntNumber();
        int[] numbers = new int[arrayLength];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = i;
        }
        int[] reversedNumbers = getReverseNumbers(numbers, numbers.length, new int[numbers.length]);
        println(Arrays.toString(reversedNumbers));
    }

    static int[] getReverseNumbers(int[] numbers, int arrayLength, int[] result) {
        if (arrayLength > 0) {
            result[numbers.length - arrayLength] = numbers[arrayLength - 1];
            return getReverseNumbers(numbers, arrayLength - 1, result);
        } else {
            return result;
        }
    }
}
