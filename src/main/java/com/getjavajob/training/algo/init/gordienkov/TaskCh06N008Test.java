package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh06N008.fillTheArray;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh06N008.getNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        testGetNumbers();
    }

    private static void testGetNumbers() {
        int[] numbers = new int[25];
        fillTheArray(numbers);
        assertEquals("TaskCh06N008Test.testGetNumbers", "1 4 9 16 25", getNumbers(numbers));
    }
}
