package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        testResultFirstTeamWinner();
        testResultSecondTeamWinner();
        testResultDraw();
        testScore();
    }

    private static void testResultFirstTeamWinner() {
        Game game = new Game();
        game.setNameOfTeam1("First");
        game.setNameOfTeam2("Second");
        game.setGameOver(false);
        game.setScoreOfTeam1(1);
        assertEquals("TaskCh06N087Test.testResultFirstTeamWinner", "First 1 : 0 Second \n" + "Winner: First", game.result());
    }

    private static void testResultSecondTeamWinner() {
        Game game = new Game();
        game.setNameOfTeam1("First");
        game.setNameOfTeam2("Second");
        game.setGameOver(false);
        game.setScoreOfTeam1(1);
        game.setScoreOfTeam2(2);
        assertEquals("TaskCh06N087Test.testResultSecondTeamWinner", "First 1 : 2 Second \n" + "Winner: Second", game.result());
    }

    private static void testResultDraw() {
        Game game = new Game();
        game.setNameOfTeam1("First");
        game.setNameOfTeam2("Second");
        game.setGameOver(false);
        game.setScoreOfTeam1(2);
        game.setScoreOfTeam2(2);
        assertEquals("TaskCh06N087Test.testResultDraw", "First 2 : 2 Second \n" + "Draw", game.result());
    }

    private static void testScore() {
        Game game = new Game();
        game.setNameOfTeam1("First");
        game.setNameOfTeam2("Second");
        game.setGameOver(false);
        game.setScoreOfTeam1(2);
        game.setScoreOfTeam2(2);
        assertEquals("TaskCh06N087Test.testScore", "First 2 : 2 Second", game.score());
    }
}
