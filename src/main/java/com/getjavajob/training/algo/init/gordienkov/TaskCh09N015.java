package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.*;

public class TaskCh09N015 {
    public static void main(String[] args) {
        println("Enter word");
        String word = readLine();
        println("Enter symbol number");
        int symbol = getIntNumberInRange(0, word.length() + 1);
        println(getChar(word, symbol));
    }

    static char getChar(String word, int symbol) {
        return word.charAt(symbol - 1);
    }
}
