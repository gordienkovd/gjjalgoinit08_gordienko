package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh09N107.replaceLetters;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N107Test {
    public static void main(String[] args) {
        testReplaceLetters();
    }

    private static void testReplaceLetters() {
        assertEquals("TaskCh09N107Test.testReplaceLetters", "lostota", replaceLetters("lastoto"));
    }
}
