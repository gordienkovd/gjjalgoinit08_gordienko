package com.getjavajob.training.algo.init.gordienkov;

import java.util.Arrays;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static java.lang.System.*;

public class TaskCh11N245 {
    public static void main(String[] args) {
        int[] numbers = new int[]{2, -1, 3, 4, -10, -5, 5, 6, 7, 8, -6};
        println(Arrays.toString(sortNumbers(numbers)));
    }

    static int[] sortNumbers(int[] numbers) {
        int[] minusNumbers = new int[0];
        int[] plusNumbers = new int[0];
        int[] resultNumbers = new int[numbers.length];
        for (int number : numbers) {
            if (number < 0) {
                int[] expr1 = new int[minusNumbers.length + 1];
                arraycopy(minusNumbers, 0, expr1, 0, minusNumbers.length);
                expr1[expr1.length - 1] = number;
                minusNumbers = expr1;
            } else {
                int[] expr1 = new int[plusNumbers.length + 1];
                arraycopy(plusNumbers, 0, expr1, 0, plusNumbers.length);
                expr1[expr1.length - 1] = number;
                plusNumbers = expr1;
            }
        }
        arraycopy(minusNumbers, 0, resultNumbers, 0, minusNumbers.length);
        arraycopy(plusNumbers, 0, resultNumbers, minusNumbers.length, plusNumbers.length);
        return resultNumbers;
    }
}
