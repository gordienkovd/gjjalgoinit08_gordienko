package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh12N024.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N024Test {
    public static void main(String[] args) {
        testGetVariantA();
        testGetVariantB();
    }

    private static void testGetVariantA() {
        int[][] matrix = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("TaskCh12N024Test.testGetVariantA", matrix, getVariantA(new int[6][6]));
    }

    private static void testGetVariantB() {
        int[][] matrix = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5}
        };
        assertEquals("TaskCh12N024Test.testGetVariantB", matrix, getVariantB(new int[6][6]));
    }
}
