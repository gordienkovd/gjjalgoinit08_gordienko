package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh04N033 {
    public static void main(String[] args) {
        println("Enter a natural number");
        int naturalNumber = getIntNumber();
        println(isEven(naturalNumber) ? "even" : "odd");
    }

    static boolean isEven(int naturalNumber) {
        return naturalNumber % 2 == 0;
    }
}
