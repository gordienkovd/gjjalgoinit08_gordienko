package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh02N013.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverseNum();
    }

    private static void testReverseNum() {
        assertEquals("TaskCh02N013Test.testReverseNum", 321, reverseNum(123));
    }
}
