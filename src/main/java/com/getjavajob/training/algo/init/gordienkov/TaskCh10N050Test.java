package com.getjavajob.training.algo.init.gordienkov;


import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N050.getFuncOfAck;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {
    public static void main(String[] args) {
        testGetFuncOfAck();
    }

    private static void testGetFuncOfAck() {
        assertEquals("TaskCh10N050Test.testGetFuncOfAck", 3, getFuncOfAck(1, 1));
    }
}
