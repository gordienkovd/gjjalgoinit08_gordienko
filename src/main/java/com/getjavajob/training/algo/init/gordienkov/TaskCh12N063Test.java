package com.getjavajob.training.algo.init.gordienkov;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh12N063.getAverageNumOfStudents;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {
    public static void main(String[] args) {
        testGetAverageNumOfStudents();
    }

    private static void testGetAverageNumOfStudents() {
        int[][] matrix = new int[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16},
                {17, 18, 19, 20},
                {21, 22, 23, 24},
                {25, 26, 27, 28},
                {29, 30, 31, 32},
                {33, 34, 35, 36},
                {37, 38, 39, 40},
                {41, 42, 43, 44}
        };
        Map<Integer, Integer> expected = new HashMap<>(11);
        expected.put(1, 2);
        expected.put(2, 6);
        expected.put(3, 10);
        expected.put(4, 14);
        expected.put(5, 18);
        expected.put(6, 22);
        expected.put(7, 26);
        expected.put(8, 30);
        expected.put(9, 34);
        expected.put(10, 38);
        expected.put(11, 42);
       assertEquals("TaskCh12N063Test.testGetAverageNumOfStudents", expected, getAverageNumOfStudents(matrix, 4, 11));
    }
}
