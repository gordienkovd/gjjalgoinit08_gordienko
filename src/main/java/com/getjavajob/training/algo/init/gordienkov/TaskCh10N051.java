package com.getjavajob.training.algo.init.gordienkov;

import java.io.IOException;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N051 {
    public static void main(String[] args) throws IOException {
        getProcedure1(5);
        println("");
        getProcedure2(5);
        println("");
        getProcedure3(5);
    }

    private static int getProcedure1(int num) {
        if (num > 0) {
            println(num);
            getProcedure1(num - 1);
        }
        return num;
    }

    private static int getProcedure2(int num) {
        if (num > 0) {
            getProcedure2(num - 1);
            println(num);
        }
        return num;
    }

    private static int getProcedure3(int num) {
        if (num > 0) {
            println(num);
            getProcedure3(num - 1);
            println(num);
        }
        return num;
    }
}