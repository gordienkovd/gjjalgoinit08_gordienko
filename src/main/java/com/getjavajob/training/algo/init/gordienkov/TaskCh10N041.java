package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N041 {
    public static void main(String[] args) {
        println("Enter natural number");
        int num = getIntNumber();
        println(getFactorial(num));
    }

    static int getFactorial(int num) {
        if (num == 1) {
            return 1;
        } else {
            return getFactorial(num - 1) * num;
        }
    }
}
