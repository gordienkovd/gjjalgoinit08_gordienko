package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh09N185.checkParentheses;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh09N185.getPosOrSumParentheses;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N185Test {
    public static void main(String[] args) {
        testCheckParenthesesTrue();
        testCheckParenthesesFalse();
        testGetPosOrSumParenthesesSum();
        testGetPosOrSumParenthesesPos();
    }

    private static void testCheckParenthesesTrue() {
        assertEquals("TaskCh09N185Test.testCheckParenthesesTrue", true, checkParentheses("(12+3)-10+(11-10)"));
    }

    private static void testCheckParenthesesFalse() {
        assertEquals("TaskCh09N185Test.testCheckParenthesesFalse", false, checkParentheses("))(x + y)(("));
    }

    private static void testGetPosOrSumParenthesesSum() {
        assertEquals("TaskCh09N185Test.testGetPosOrSumParenthesesSum", "The amount of left parentheses = 3", getPosOrSumParentheses("((12+3)-10+(11-10)"));
    }

    private static void testGetPosOrSumParenthesesPos() {
        assertEquals("TaskCh09N185Test.testGetPosOrSumParenthesesPos", "The position of the first wrong right parenthesis = 6", getPosOrSumParentheses("(12+3))-10+(11-10)"));
    }
}
