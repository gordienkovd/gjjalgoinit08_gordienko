package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh04N036.knowColorOfTrafficLight;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N036Test {
    public static void main(String[] args) {
        testKnowColorOfTrafficLightRed();
        testKnowColorOfTrafficLightGreen();
    }

    private static void testKnowColorOfTrafficLightRed() {
        assertEquals("TaskCh04N036Test.testKnowColorOfTrafficLightRed", "Red", knowColorOfTrafficLight(3));
    }

    private static void testKnowColorOfTrafficLightGreen() {
        assertEquals("TaskCh04N036Test.testKnowColorOfTrafficLightGreen", "Green", knowColorOfTrafficLight(5));
    }
}
