package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readLine;

public class TaskCh09N185 {
    public static void main(String[] args) {
        println("Enter a mathematical expression");
        String mathExpression = readLine();
        if (!checkParentheses(mathExpression)) {
            println("No");
            println(getPosOrSumParentheses(mathExpression));
        } else {
            println("Yes");
        }
    }

    static boolean checkParentheses(String mathExpression) {
        if (mathExpression.indexOf(')') > mathExpression.indexOf('('))
            if (mathExpression.length() - mathExpression.replaceAll("\\(", "").length() == mathExpression.length() - mathExpression.replaceAll("\\)", "").length())
                return true;
            else {
                return false;
            }
        else {
            return false;
        }
    }

    static String getPosOrSumParentheses(String mathExpression) {
        int posOfParenthesis = 0;
        if (mathExpression.length() - mathExpression.replaceAll("\\(", "").length() > mathExpression.length() - mathExpression.replaceAll("\\)", "").length()) {
            return "The amount of left parentheses = " + (mathExpression.length() - mathExpression.replaceAll("\\(", "").length());
        } else {
            int exprCount = 0;
            for (int i = 0; i < mathExpression.length(); i++) {
                if (mathExpression.charAt(i) == '(') {
                    exprCount++;
                } else if (mathExpression.charAt(i) == ')') {
                    exprCount--;
                    if (exprCount < 0) {
                        posOfParenthesis = i;
                        break;
                    }
                }
            }
            return "The position of the first wrong right parenthesis = " + posOfParenthesis;
        }
    }
}