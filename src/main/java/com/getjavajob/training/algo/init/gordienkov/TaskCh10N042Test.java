package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N042.getNumDegree;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N042Test {
    public static void main(String[] args) {
        testGetNumDegree();
    }

    private static void testGetNumDegree() {
        assertEquals("TaskCh10N042Test.testGetNumDegree", 27, getNumDegree(3.0, 3));
    }
}
