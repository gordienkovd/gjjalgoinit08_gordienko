package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N049 {
    public static void main(String[] args) {
        println("Enter the size of the array");
        int lengthOfArray = getIntNumber();
        int[] numbers = new int[lengthOfArray];
        for (int i = 0; i < lengthOfArray; i++) {
            numbers[i] = i;
        }
        println(maxIndexOfArr(numbers, 0, 0, 0));
    }

    static int maxIndexOfArr(int[] numbers, int count, int value, int index) {
        if (count == numbers.length) {
            return index;
        } else {
            if (value < numbers[count]) {
                value = numbers[count];
                index = count;
            }
            count++;
            return maxIndexOfArr(numbers, count, value, index);
        }
    }
}
