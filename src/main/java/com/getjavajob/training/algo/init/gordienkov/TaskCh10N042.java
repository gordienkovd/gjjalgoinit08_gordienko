package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readDouble;

public class TaskCh10N042 {
    public static void main(String[] args) {
        println("Enter double number");
        double num = readDouble();
        println("Enter degree");
        int degree = getIntNumber();
        println(getNumDegree(num, degree));
    }

    static double getNumDegree(double num, int degree) {
        if (degree == 1) {
            return num;
        } else {
            return getNumDegree(num, degree - 1) * num;
        }
    }
}
