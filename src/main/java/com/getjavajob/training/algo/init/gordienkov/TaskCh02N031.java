package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumberInRange;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh02N031 {
    public static void main(String[] args) {
        int num = getIntNumberInRange(100, 200);
        println(getInitialNumber(num));
    }

    static int getInitialNumber(int num) {
        int firstNumber = num / 100;
        int secondNumber = (num % 100) / 10;
        int thirdNumber = num - firstNumber * 100 - secondNumber * 10;
        return firstNumber * 100 + thirdNumber * 10 + secondNumber;
    }
}
