package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh01N003 {
    public static void main(String[] args) {
        println("You entered a number " + getIntNumber());
    }
}
