package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N043 {
    public static void main(String[] args) {
        println("Enter natural number");
        int num = getIntNumber();
        println(getSumOfDigits(num));
        println(getAmountOfDigits(num));
    }

    static int getSumOfDigits(int num) {
        if (num / 10 == 0) {
            return num;
        } else {
            return getSumOfDigits(num / 10) + num % 10;
        }
    }

    static int getAmountOfDigits(int num) {
        if (num / 10 == 0) {
            return 1;
        } else {
            return getAmountOfDigits(num / 10) + 1;
        }
    }
}
