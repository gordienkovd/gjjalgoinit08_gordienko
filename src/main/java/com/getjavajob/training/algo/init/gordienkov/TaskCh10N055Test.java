package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N055.convertToNumSystem;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        testConvertToNumSystem();
    }

    private static void testConvertToNumSystem() {
        assertEquals("TaskCh10N055Test.testConvertToNumSystem", "A0", convertToNumSystem(160, 16, ""));
    }
}
