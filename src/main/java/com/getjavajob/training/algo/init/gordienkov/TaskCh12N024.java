package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.printMatrix;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh12N024 {
    public static void main(String[] args) {
        int[][] matrix = getVariantA(new int[6][6]);
        int[][] matrix1 = getVariantB(new int[6][6]);
        printMatrix(matrix);
        println("");
        printMatrix(matrix1);
    }

    static int[][] getVariantA(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i == 0 || j == 0) {
                    matrix[i][j] = 1;
                } else {
                    matrix[i][j] = matrix[i][j - 1] + matrix[i - 1][j];
                }
            }
        }
        return matrix;
    }

    static int[][] getVariantB(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i == 0) {
                    matrix[i][j] = j + 1;
                } else if (matrix[i - 1][j] == matrix.length) {
                    matrix[i][j] = 1;
                } else {
                    matrix[i][j] = matrix[i - 1][j] + 1;
                }
            }
        }
        return matrix;
    }
}
