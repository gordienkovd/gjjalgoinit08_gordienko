package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N052.getReverseNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        testGetReverseNumber();
    }

    private static void testGetReverseNumber() {
        assertEquals("TaskCh10N052Test.testGetReverseNumber", 321, getReverseNumber(123, 0));
    }
}
