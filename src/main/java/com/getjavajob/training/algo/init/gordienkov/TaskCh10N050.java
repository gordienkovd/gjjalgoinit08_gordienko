package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N050 {
    public static void main(String[] args) {
        println("Enter the first number of functions of Ackermann");
        int m = getIntNumber();
        println("Enter the second number of functions of Ackermann");
        int n = getIntNumber();
        println(getFuncOfAck(n, m));
    }

    static int getFuncOfAck(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return getFuncOfAck(n - 1, 1);
        } else {
            return getFuncOfAck(n - 1, getFuncOfAck(n, m - 1));
        }
    }
}
