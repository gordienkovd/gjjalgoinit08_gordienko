package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.*;

public class TaskCh10N055 {
    public static void main(String[] args) {
        int num = getIntNumber();
        int numSystem = getIntNumberInRange(1, 17);
        println(convertToNumSystem(num, numSystem, ""));
    }

    static String convertToNumSystem(int num, int numSystem, String result) {
        if (num >= numSystem) {
            result += checkNumber(num % numSystem);
            return convertToNumSystem(num / numSystem, numSystem, result);
        } else {
            result += checkNumber(num % numSystem);
            StringBuilder reversedResult = new StringBuilder(result).reverse();
            return reversedResult.toString();
        }
    }

    private static String checkNumber(int number) {
        switch (number) {
            case 10 :
                return "A";
            case 11 :
                return "B";
            case 12 :
                return "C";
            case 13 :
                return "D";
            case 14:
                return "E";
            case 15 :
                return "F";
            default:
                return Integer.toString(number);
        }
    }
}
