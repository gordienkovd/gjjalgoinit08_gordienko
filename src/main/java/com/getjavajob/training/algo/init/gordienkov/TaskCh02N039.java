package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumberInRange;
import static com.getjavajob.training.algo.util.ConsoleHelper.isInRange;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh02N039 {
    public static void main(String[] args) {
        println(getDegree(getIntNumberInRange(-1, 24), getIntNumberInRange(-1, 60), getIntNumberInRange(-1, 60)));
    }

    static int getDegree(int hours, int minutes, int seconds) {
        int resultHours = hours;
        if (hours > 11) {
            resultHours -= 12;
        }
        int degree = (resultHours * 3600 + minutes * 60 + seconds) / 120;
        if (!isInRange(degree, -1, 181)) {
            degree = degree - (degree - 180) * 2;
        }
        return degree;
    }
}
