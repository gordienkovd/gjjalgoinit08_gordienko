package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readLine;

public class TaskCh09N017 {
    public static void main(String[] args) {
        println("Enter word");
        String word = readLine();
        println(compareLetters(word) ? "The first and last letters the same" : "The first and the last letters are different");
    }

    static boolean compareLetters(String word) {
        return word.toLowerCase().charAt(0) == word.toLowerCase().charAt(word.length() - 1);
    }
}
