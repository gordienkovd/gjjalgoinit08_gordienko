package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N049.maxIndexOfArr;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {
    public static void main(String[] args) {
        testMaxIndexOfArr();
    }

    private static void testMaxIndexOfArr() {
        assertEquals("TaskCh10N049Test.testMaxIndexOfArr", 7, maxIndexOfArr(new int[]{3, 1, 2, 3, 4, 5, 0, 7, -2, -1,}, 0, 0, 0));
    }
}
