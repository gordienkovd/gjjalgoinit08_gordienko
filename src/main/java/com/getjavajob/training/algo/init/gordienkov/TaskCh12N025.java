package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.printMatrix2;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh12N025 {
    public static void main(String[] args) {
        printAllMatrix();
    }

    private static int[][] getVariantA(int[][] matrix, int height, int width) {
        int count = 1;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                matrix[i][j] = count;
                count++;
            }
        }
        return matrix;
    }

    private static int[][] getVariantB(int[][] matrix, int height, int width) {
        int count = 1;
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < height; i++) {
                matrix[i][j] = count;
                count++;
            }
        }
        return matrix;
    }

    private static int[][] getVariantV(int[][] matrix, int height, int width) {
        int count = 1;
        for (int i = 0; i < height; i++) {
            for (int j = width - 1; j != -1; j--) {
                matrix[i][j] = count;
                count++;
            }
        }
        return matrix;
    }

    private static int[][] getVariantG(int[][] matrix, int height, int width) {
        int count = 1;
        for (int j = 0; j < width; j++) {
            for (int i = height - 1; i != -1; i--) {
                matrix[i][j] = count;
                count++;
            }
        }
        return matrix;
    }

    private static int[][] getVariantD(int[][] matrix, int height, int width) {
        int count = 1;
        boolean direction = true;
        for (int i = 0; i < height; i++) {
            if (direction) {
                for (int j = 0; j < width; j++) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = false;
            } else {
                for (int j = width - 1; j != -1; j--) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = true;
            }
        }
        return matrix;
    }

    private static int[][] getVariantE(int[][] matrix, int height, int width) {
        int count = 1;
        boolean direction = true;
        for (int j = 0; j < width; j++) {
            if (direction) {
                for (int i = 0; i < height; i++) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = false;
            } else {
                for (int i = height - 1; i != -1; i--) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = true;
            }
        }
        return matrix;
    }

    private static int[][] getVariantZH(int[][] matrix, int height, int width) {
        int count = 1;
        for (int i = height - 1; i != -1; i--) {
            for (int j = 0; j < width; j++) {
                matrix[i][j] = count;
                count++;
            }
        }
        return matrix;
    }

    private static int[][] getVariantZ(int[][] matrix, int height, int width) {
        int count = 1;
        for (int j = width - 1; j != -1; j--) {
            for (int i = 0; i < height; i++) {
                matrix[i][j] = count;
                count++;
            }
        }
        return matrix;
    }

    private static int[][] getVariantI(int[][] matrix, int height, int width) {
        int count = 1;
        for (int i = height - 1; i != -1; i--) {
            for (int j = width - 1; j != -1; j--) {
                matrix[i][j] = count;
                count++;
            }
        }
        return matrix;
    }

    private static int[][] getVariantK(int[][] matrix, int height, int width) {
        int count = 1;
        for (int j = width - 1; j != -1; j--) {
            for (int i = height - 1; i != -1; i--) {
                matrix[i][j] = count;
                count++;
            }
        }
        return matrix;
    }

    private static int[][] getVariantL(int[][] matrix, int height, int width) {
        int count = 1;
        boolean direction = true;
        for (int i = height - 1; i != -1; i--) {
            if (direction) {
                for (int j = 0; j < width; j++) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = false;
            } else {
                for (int j = width - 1; j != -1; j--) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = true;
            }
        }
        return matrix;
    }

    private static int[][] getVariantM(int[][] matrix, int height, int width) {
        int count = 1;
        boolean direction = false;
        for (int i = 0; i < height; i++) {
            if (direction) {
                for (int j = 0; j < width; j++) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = false;
            } else {
                for (int j = width - 1; j != -1; j--) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = true;
            }
        }
        return matrix;
    }

    private static int[][] getVariantN(int[][] matrix, int height, int width) {
        int count = 1;
        boolean direction = true;
        for (int j = width - 1; j != -1; j--) {
            if (direction) {
                for (int i = 0; i < height; i++) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = false;
            } else {
                for (int i = height - 1; i != -1; i--) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = true;
            }
        }
        return matrix;
    }

    private static int[][] getVariantO(int[][] matrix, int height, int width) {
        int count = 1;
        boolean direction = false;
        for (int j = 0; j < width; j++) {
            if (direction) {
                for (int i = 0; i < height; i++) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = false;
            } else {
                for (int i = height - 1; i != -1; i--) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = true;
            }
        }
        return matrix;
    }

    private static int[][] getVariantP(int[][] matrix, int height, int width) {
        int count = 1;
        boolean direction = false;
        for (int i = height - 1; i != -1; i--) {
            if (direction) {
                for (int j = 0; j < width; j++) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = false;
            } else {
                for (int j = width - 1; j != -1; j--) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = true;
            }
        }
        return matrix;
    }

    private static int[][] getVariantR(int[][] matrix, int height, int width) {
        int count = 1;
        boolean direction = false;
        for (int j = width - 1; j != -1; j--) {
            if (direction) {
                for (int i = 0; i < height; i++) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = false;
            } else {
                for (int i = height - 1; i != -1; i--) {
                    matrix[i][j] = count;
                    count++;
                }
                direction = true;
            }
        }
        return matrix;
    }

    private static void printAllMatrix() {
        int[][] matrix = new int[12][10];
        int[][] matrix1 = new int[10][12];
        println("\nVariantA\n");
        printMatrix2(getVariantA(matrix, 12, 10), 12, 10);
        println("\nVariantB\n");
        printMatrix2(getVariantB(matrix, 12, 10), 12, 10);
        println("\nVariantV\n");
        printMatrix2(getVariantV(matrix, 12, 10), 12, 10);
        println("\nVariantG\n");
        printMatrix2(getVariantG(matrix, 12, 10), 12, 10);
        println("\nVariantD\n");
        printMatrix2(getVariantD(matrix1, 10, 12), 10, 12);
        println("\nVariantE\n");
        printMatrix2(getVariantE(matrix, 12, 10), 12, 10);
        println("\nVariantZH\n");
        printMatrix2(getVariantZH(matrix, 12, 10), 12, 10);
        println("\nVariantZ\n");
        printMatrix2(getVariantZ(matrix, 12, 10), 12, 10);
        println("\nVariantI\n");
        printMatrix2(getVariantI(matrix, 12, 10), 12, 10);
        println("\nVariantK\n");
        printMatrix2(getVariantK(matrix, 12, 10), 12, 10);
        println("\nVariantL\n");
        printMatrix2(getVariantL(matrix, 12, 10), 12, 10);
        println("\nVariantM\n");
        printMatrix2(getVariantM(matrix, 12, 10), 12, 10);
        println("\nVariantN\n");
        printMatrix2(getVariantN(matrix, 12, 10), 12, 10);
        println("\nVariantO\n");
        printMatrix2(getVariantO(matrix, 12, 10), 12, 10);
        println("\nVariantP\n");
        printMatrix2(getVariantP(matrix, 12, 10), 12, 10);
        println("\nVariantR\n");
        printMatrix2(getVariantR(matrix, 12, 10), 12, 10);
    }
}
