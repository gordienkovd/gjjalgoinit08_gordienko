package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readLine;

public class TaskCh09N166 {
    public static void main(String[] args) {
        println("Enter sentence");
        String sentence = readLine();
        println(replaceWords(sentence));
    }

    static String replaceWords(String sentence) {
        String[] words = sentence.split(" ");
        StringBuilder result = new StringBuilder("");
        String exprWord = words[words.length - 1];
        words[words.length - 1] = words[0];
        words[0] = exprWord;
        for (String word : words) {
            result.append(word).append(' ');
        }
        return result.toString().trim();
    }
}
