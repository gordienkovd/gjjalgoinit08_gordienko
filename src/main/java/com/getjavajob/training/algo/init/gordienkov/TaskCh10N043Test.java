package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N043.getAmountOfDigits;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh10N043.getSumOfDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {
    public static void main(String[] args) {
        testGetSumOfDigits();
        testGetAmountOfDigits();
    }

    private static void testGetSumOfDigits() {
        assertEquals("TaskCh10N043Test.testGetSumOfDigits", 6, getSumOfDigits(123));
    }

    private static void testGetAmountOfDigits() {
        assertEquals("TaskCh10N043Test.testGetAmountOfDigits", 3, getAmountOfDigits(123));
    }
}
