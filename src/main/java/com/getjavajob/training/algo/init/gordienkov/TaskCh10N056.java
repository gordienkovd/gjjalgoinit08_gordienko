package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N056 {
    public static void main(String[] args) {
        println("Enter number");
        int num = getIntNumber();
        println(isPrimeNumber(num, 2) ? "Yes" : "No");
    }

    static boolean isPrimeNumber(int num, int divider) {
        if (num >= 2) {
            if (divider >= num / 2 || isPrimeNumber(num, divider + 1)) {
                if (num % 2 != 0) {
                    return true;
                } else {
                    if (num == 2) {
                        return true;
                    }
                }
            }
            else {
                if (num == 2) {
                    return true;
                }
            }
        }
        return false;
    }
}