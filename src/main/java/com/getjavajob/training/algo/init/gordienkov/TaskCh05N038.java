package com.getjavajob.training.algo.init.gordienkov;


import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh05N038 {
    public static void main(String[] args) {
        println("Enter the number of steps");
        int steps = getIntNumber();
        println("The distance to the house = " + getDistanceToTheHouse(steps));
        println("Passed distance = " + getDistance(steps));
    }

    static double getDistance(int steps) {
        double result = 0;
        for (int i = 1; i < steps + 1; i++) {
            result += 1.0 / i;
        }
        return result;
    }

    static double getDistanceToTheHouse(int steps) {
        double result = 0.0;
        for (int i = 1; i < steps + 1; i++) {
            if (i % 2 == 0) {
                result -= 1.0 / i;
            } else {
                result += 1.0 / i;
            }
        }
        return result;
    }
}
