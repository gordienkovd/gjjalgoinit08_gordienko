package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.println;
import static com.getjavajob.training.algo.util.ConsoleHelper.readDouble;

public class TaskCh05N010 {
    public static void main(String[] args) {
        println("Enter USD/RUB");
        double curseOfDollar = readDouble();
        double[] dollars = new double[20];
        for (int i = 0; i < dollars.length; i++) {
            dollars[i] = i + 1;
        }
        double[] rubles = getDollarCourse(dollars, curseOfDollar);
        for (int i = 0; i < dollars.length; i++) {
            println(String.format("%.2f$ = %.2f\u20BD", dollars[i], rubles[i]));
        }
    }

    static double[] getDollarCourse(double[] dollars, double curseOfDollar) {
        double[] result = new double[dollars.length];
        for (int i = 0; i < dollars.length; i++) {
            result[i] = dollars[i] * curseOfDollar;
        }
        return result;
    }
}
