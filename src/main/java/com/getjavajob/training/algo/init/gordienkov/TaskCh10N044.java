package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo.util.ConsoleHelper.println;

public class TaskCh10N044 {
    public static void main(String[] args) {
        println("Enter natural number");
        int num = getIntNumber();
        println(getDigitalRoot(num));
    }

    static int getDigitalRoot(int num) {
        if (num / 10 == 0) {
            return num;
        } else {
            return getDigitalRoot(getDigitalRoot(num / 10) + num % 10);
        }
    }
}
