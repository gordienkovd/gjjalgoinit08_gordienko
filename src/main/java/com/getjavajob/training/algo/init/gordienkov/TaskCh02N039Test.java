package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh02N039.getDegree;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N039Test {
    public static void main(String[] args) {
        testGetDegreeBeforeNoon();
        testGetDegreeAfterNoon();
    }

    private static void testGetDegreeBeforeNoon() {
        assertEquals("TaskCh02N031Test.testGetDegreeBeforeNoon", 55, getDegree(10, 10, 10));
    }

    private static void testGetDegreeAfterNoon() {
        assertEquals("TaskCh02N031Test.testGetDegreeAfterNoon", 95, getDegree(15, 10, 10));
    }
}