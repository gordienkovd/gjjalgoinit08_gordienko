package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh12N234.deleteColumn;
import static com.getjavajob.training.algo.init.gordienkov.TaskCh12N234.deleteRow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {
    public static void main(String[] args) {
        testDeleteRow();
        testDeleteColumn();
    }

    private static void testDeleteRow() {
        int height = 11;
        int width = 4;
        int[][] matrix = new int[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16},
                {17, 18, 19, 20},
                {21, 22, 23, 24},
                {25, 26, 27, 28},
                {29, 30, 31, 32},
                {33, 34, 35, 36},
                {37, 38, 39, 40},
                {41, 42, 43, 44}
        };
        int[][] matrixDeleteRow = new int[][]{
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16},
                {17, 18, 19, 20},
                {21, 22, 23, 24},
                {25, 26, 27, 28},
                {29, 30, 31, 32},
                {33, 34, 35, 36},
                {37, 38, 39, 40},
                {41, 42, 43, 44},
                {0, 0, 0, 0}
        };
        assertEquals("TaskCh12N234Test.testDeleteRow", matrixDeleteRow, deleteRow(matrix, height, width, 1));
    }

    private static void testDeleteColumn() {
        int height = 11;
        int width = 4;
        int[][] matrix = new int[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16},
                {17, 18, 19, 20},
                {21, 22, 23, 24},
                {25, 26, 27, 28},
                {29, 30, 31, 32},
                {33, 34, 35, 36},
                {37, 38, 39, 40},
                {41, 42, 43, 44}
        };
        int[][] matrixDeleteColumn = new int[][]{
                {2, 3, 4, 0},
                {6, 7, 8, 0},
                {10, 11, 12, 0},
                {14, 15, 16, 0},
                {18, 19, 20, 0},
                {22, 23, 24, 0},
                {26, 27, 28, 0},
                {30, 31, 32, 0},
                {34, 35, 36, 0},
                {38, 39, 40, 0},
                {42, 43, 44, 0}
        };
        assertEquals("TaskCh12N234Test.testDeleteColumn", matrixDeleteColumn, deleteColumn(matrix, height, width, 1));
    }
}
