package com.getjavajob.training.algo.init.gordienkov;

import static com.getjavajob.training.algo.init.gordienkov.TaskCh09N022.getHalfOfTheWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N022Test {
    public static void main(String[] args) {
        testGetHalfOfTheWord();
    }

    private static void testGetHalfOfTheWord() {
        assertEquals("TaskCh09N022Test.getHalfOfTheWord", "te", getHalfOfTheWord("test"));
    }
}
